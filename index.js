const bodyParser = require("body-parser");
const express = require("express");
const app = express();
var mongo = require("mongodb");

var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/mydb";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
	if (err) throw err;
	var dbo = db.db("db_name");
	dbo.createCollection("employee", function(err, res) {
		if (err) throw err;
		console.log("Collection created!");
		db.close();
	});
	dbo.createCollection("register", function(err, res) {
		if (err) throw err;
		console.log("Collection created!");
		db.close();
	});
});

app.get("/", function(req, res) {
	res.send("Hello World");
});

app.listen(8080, () => {
	console.log("Start server at port 8080.");
});

app.post("/employee", function(req, res) {
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		if (err) res.send(err);
		var dbo = db.db("db_name");
		dbo.collection("employee").insertOne(req.body, function(err, res2) {
			if (err) res.send(err);
			console.log("Inserted");
			res.send(req.body);
			db.close();
		});
	});
});

app.get("/employee", function(req, res) {
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		if (err) res.send(err);
		var dbo = db.db("db_name");
		dbo
			.collection("employee")
			.find({})
			.toArray(function(err, result) {
				if (err) res.send(err);
				console.log(result);
				res.send(result);
				db.close();
			});
	});
});

app.get("/employee/:employee_id", function(req, res) {
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		if (err) res.send(err);
		var dbo = db.db("db_name");
		var query = { employee_id: req.params.employee_id };
		dbo
			.collection("employee")
			.find(query)
			.toArray(function(err, result) {
				if (err) res.send(err);
				console.log(result);
				res.send(result);
				db.close();
			});
	});
});

app.put("/employee/:employee_id", function(req, res) {
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		if (err) res.send(err);
		var dbo = db.db("db_name");
		var newvalues = { $set: req.body };
		dbo
			.collection("employee")
			.updateOne({ employee_id: req.params.employee_id }, newvalues, function(
				err2,
				res2
			) {
				if (err2) res.send(err2);
				console.log("Updated employee_id: " + req.params.employee_id);
				res.send("Updated employee_id: " + req.params.employee_id);
				db.close();
			});
	});
});

app.delete("/employee/:employee_id", function(req, res) {
	MongoClient.connect(url, function(err, db) {
		if (err) res.send(err);
		var dbo = db.db("db_name");
		var myquery = { employee_id: req.params.employee_id };
		dbo
			.collection("employee")
			.deleteOne(myquery, { useNewUrlParser: true }, function(err, obj) {
				if (err) res.send(err);
				console.log("Deleted employee_id: " + req.params.employee_id);
				res.send("Deleted employee_id: " + req.params.employee_id);
				db.close();
			});
	});
});

// Register API

app.post("/register", function(req, res) {
	var { name, national_id, Contact_Mobile } = req.body;
	var validation = [];
	var validLogic = [
		{
			logic: national_id.toString().length !== 13,
			errLog: `Wrong Format for National_ID`
		},
		{
			logic: Contact_Mobile.toString().length < 10,
			errLog: `Wrong Format for Contact_Mobile`
		}
	];
	validLogic.map(({ logic, errLog }) => {
		if (logic) {
			validation.push(errLog);
		}
	});

	var data = {
		...req.body,
		Userid:
			name +
			Contact_Mobile.toString().substr(
				Contact_Mobile.toString().length - 3,
				Contact_Mobile.toString().length
			)
	};

	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		if (err) res.send(err);
		var dbo = db.db("db_name");
		new Promise((resolve, reject) => {
			dbo
				.collection("register")
				.find({ Userid: data.Userid })
				.toArray(function(err, result) {
					if (err) res.send(err);
					if (result.length > 0) {
						validation.push(
							`You have Registered Already, Please check you information.`
						);
						if (validation.length > 0) res.send({ validation });
						db.close();
						reject();
					}
					resolve();
				});
		}).then(() => {
			dbo.collection("register").insertOne(data, function(err, result) {
				if (err) res.send(err);
				console.log("Inserted");
				res.send({ name: data.name, Userid: data.Userid });
				db.close();
			});
		});
	});
});

app.get("/register/:Userid", function(req, res) {
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		if (err) res.send(err);
		var dbo = db.db("db_name");
		var query = { Userid: req.params.Userid };
		dbo
			.collection("register")
			.find(query)
			.toArray(function(err, result) {
				if (err) res.send(err);
				console.log(result);
				res.send(result);
				db.close();
			});
	});
});

app.get("/register/getinfo/:national_id", function(req, res) {
	MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		if (err) res.send(err);
		var dbo = db.db("db_name");
		var query = { national_id: req.params.national_id };
		dbo
			.collection("register")
			.find(query)
			.toArray(function(err, result) {
				if (err) res.send(err);
				var data = [].concat(result).map(({ name, address, Contact_Mobile }) => {
					[3, 6].map((index, count) => {
						Contact_Mobile =
							Contact_Mobile.substr(0, index + count) +
							"-" +
							Contact_Mobile.substr(index + count);
					});
					return { name, address, Contact_Mobile };
				});
				res.send(data);
				db.close();
			});
	});
});
